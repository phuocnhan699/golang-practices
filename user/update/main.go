package main

import (
	"fmt"
	"time"

	"database/sql"
	"encoding/json"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/google/uuid"
	_ "github.com/lib/pq"
)

// BodyRequest is our self-made struct to process JSON request from Client
type BodyRequest struct {
	RequestId   string      `json:"requestId"`
	RequestTime string      `json:"requestTime"`
	Data        DataRequest `json:"data"`
}

type DataRequest struct {
	UserName string `json:"username"`
	Name     string `json:"name"`
	Phone    string `json:"phone"`
}

// BodyResponse is our self-made struct to build response for Client
type BodyResponse struct {
	ResponseId      string `json:"responseId" binding:"required"`
	ResponseTime    string `json:"responseTime"`
	ResponseCode    string `json:"responseCode"`
	ResponseMessage string `json:"responseMessage"`
}

// Handler function Using AWS Lambda Proxy Request
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	datetime := time.Now().UTC()
	// BodyRequest will be used to take the json response from client and build it
	bodyRequest := BodyRequest{
		RequestId: "",
	}

	bodyResponse := BodyResponse{
		ResponseId:      uuid.New().String(),
		ResponseTime:    datetime.Format(time.RFC3339),
		ResponseCode:    "99",
		ResponseMessage: "System Err",
	}
	response, err := json.Marshal(&bodyResponse)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: string(response), StatusCode: 200}, nil
	}
	// Unmarshal the json, return 404 if error
	err = json.Unmarshal([]byte(request.Body), &bodyRequest)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: string(response), StatusCode: 200}, nil
	}

	//verify uuid not null
	if bodyRequest.RequestId == "" {
		return events.APIGatewayProxyResponse{Body: "requestId can not be null", StatusCode: 200}, nil
	}

	//verify datetime format RFC3339
	// parsedTime, err := time.Parse(time.RFC3339, bodyRequest.RequestTime)
	// if err != nil {
	// 	return events.APIGatewayProxyResponse{Body: err.Error() + "parsedTime: " + parsedTime.GoString(), StatusCode: 401}, nil
	// }

	//verify hash materials
	if bodyRequest.Data.UserName == "" {
		bodyResponse = BodyResponse{
			ResponseId:      uuid.New().String(),
			ResponseTime:    datetime.Format(time.RFC3339),
			ResponseCode:    "01",
			ResponseMessage: "username can not be null",
		}
		response, err = json.Marshal(&bodyResponse)
		return events.APIGatewayProxyResponse{Body: string(response), StatusCode: 200}, nil
	}

	if bodyRequest.Data.Name == "" {
		bodyResponse = BodyResponse{
			ResponseId:      uuid.New().String(),
			ResponseTime:    datetime.Format(time.RFC3339),
			ResponseCode:    "02",
			ResponseMessage: "name can not be null",
		}
		response, err = json.Marshal(&bodyResponse)
		return events.APIGatewayProxyResponse{Body: "name can not be null", StatusCode: 200}, nil
	}

	if bodyRequest.Data.Phone == "" {
		bodyResponse = BodyResponse{
			ResponseId:      uuid.New().String(),
			ResponseTime:    datetime.Format(time.RFC3339),
			ResponseCode:    "03",
			ResponseMessage: "phone can not be null",
		}
		response, err = json.Marshal(&bodyResponse)
		return events.APIGatewayProxyResponse{Body: "phone can not be null", StatusCode: 200}, nil
	}
	//connect postgree
	const (
		host     = "rajje.db.elephantsql.com"
		port     = 5432
		user     = "yairsggo"
		password = "MbuwvGgJcC-nXskeCQnhunp8C93XC2-p"
		dbname   = "yairsggo"
	)
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	err = db.Ping()
	if err != nil {
		panic(err)
	}
	sqlStatement := `UPDATE nhanpp1 SET name = $2, phone = $3 WHERE username=$1;`
	_, err = db.Exec(sqlStatement, bodyRequest.Data.UserName, bodyRequest.Data.Name, bodyRequest.Data.Phone)
	if err != nil {
		bodyResponse = BodyResponse{
			ResponseId:      uuid.New().String(),
			ResponseTime:    datetime.Format(time.RFC3339),
			ResponseCode:    "04",
			ResponseMessage: err.Error(),
		}
		response, err = json.Marshal(&bodyResponse)
		// check err
		if err != nil {
			return events.APIGatewayProxyResponse{Body: string(response), StatusCode: 400}, nil
		}
		return events.APIGatewayProxyResponse{Body: string(response), StatusCode: 200}, nil
	}
	//
	bodyResponse = BodyResponse{
		ResponseId:      uuid.New().String(),
		ResponseTime:    datetime.Format(time.RFC3339),
		ResponseCode:    "00",
		ResponseMessage: "Success",
	}

	// Marshal the response into json bytes, if error return 404
	response, err = json.Marshal(&bodyResponse)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 404}, nil
	}

	//Returning response with AWS Lambda Proxy Response
	return events.APIGatewayProxyResponse{Body: string(response), StatusCode: 200}, nil
}

func main() {
	// Handler("{\"requestId\":\"123456\",\"requestTime\":\"123456\",\"data\":{\"username\":\"nhanne_p\",\"name\":\"PhamPhuocNhan\",\"phone\":\"0902123456\"}}")
	lambda.Start(Handler)
}
