package main

import (
	"fmt"
	"time"

	"database/sql"
	"encoding/json"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/google/uuid"
	_ "github.com/lib/pq"
)

// BodyRequest is our self-made struct to process JSON request from Client

type DataResponse struct {
	UserName string `json:"username"`
	Name     string `json:"name"`
	Phone    string `json:"phone"`
}

// BodyResponse is our self-made struct to build response for Client
type BodyResponse struct {
	ResponseId      string       `json:"responseId"`
	ResponseTime    string       `json:"responseTime"`
	ResponseCode    string       `json:"responseCode"`
	ResponseMessage string       `json:"responseMessage"`
	Data            DataResponse `json:"data"`
}

// Handler function Using AWS Lambda Proxy Request
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	datetime := time.Now().UTC()
	// BodyRequest will be used to take the json response from client and build it

	username := request.QueryStringParameters["username"]
	bodyResponse := BodyResponse{
		ResponseId:      uuid.New().String(),
		ResponseTime:    datetime.Format(time.RFC3339),
		ResponseCode:    "99",
		ResponseMessage: "System Err",
	}
	response, err := json.Marshal(&bodyResponse)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: string(response), StatusCode: 200}, nil
	}

	//verify datetime format RFC3339
	// parsedTime, err := time.Parse(time.RFC3339, bodyRequest.RequestTime)
	// if err != nil {
	// 	return events.APIGatewayProxyResponse{Body: err.Error() + "parsedTime: " + parsedTime.GoString(), StatusCode: 401}, nil
	// }

	//connect postgree
	const (
		host     = "rajje.db.elephantsql.com"
		port     = 5432
		user     = "yairsggo"
		password = "MbuwvGgJcC-nXskeCQnhunp8C93XC2-p"
		dbname   = "yairsggo"
	)
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	err = db.Ping()
	if err != nil {
		panic(err)
	}
	var name string
	var phone string
	sqlStatement := `abcSELECT username, name, phone FROM nhanpp1 WHERE username=$1`
	row := db.QueryRow(sqlStatement, username)
	defer func() {
		recover()
	}()
	switch err := row.Scan(&username, &name, &phone); err {
	case sql.ErrNoRows:
		bodyResponse = BodyResponse{
			ResponseId:      uuid.New().String(),
			ResponseTime:    datetime.Format(time.RFC3339),
			ResponseCode:    "06",
			ResponseMessage: "Not found records returned",
		}
	case nil:
		bodyResponse = BodyResponse{
			ResponseId:      uuid.New().String(),
			ResponseTime:    datetime.Format(time.RFC3339),
			ResponseCode:    "00",
			ResponseMessage: "Success",
			Data: DataResponse{
				UserName: username,
				Name:     name,
				Phone:    phone,
			},
		}
	default:
		bodyResponse = BodyResponse{
			ResponseId:      uuid.New().String(),
			ResponseTime:    datetime.Format(time.RFC3339),
			ResponseCode:    "99",
			ResponseMessage: "db error",
			Data: DataResponse{
				UserName: username,
				Name:     name,
				Phone:    phone,
			},
		}
	}
	//

	// Marshal the response into json bytes, if error return 404
	response, err = json.Marshal(&bodyResponse)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 404}, nil
	}

	//Returning response with AWS Lambda Proxy Response
	return events.APIGatewayProxyResponse{Body: string(response), StatusCode: 200}, nil
}

func main() {
	// Handler("{\"requestId\":\"123456\",\"requestTime\":\"123456\",\"data\":{\"username\":\"nhanne_p\",\"name\":\"PhamPhuocNhan\",\"phone\":\"0902123456\"}}")
	lambda.Start(Handler)
}
