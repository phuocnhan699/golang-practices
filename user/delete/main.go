package main

import (
	"fmt"
	"reflect"
	"time"

	"database/sql"
	"encoding/json"

	"github.com/aws/aws-lambda-go/events"
	"github.com/google/uuid"
	_ "github.com/lib/pq"
)

// BodyRequest is our self-made struct to process JSON request from Client

// BodyResponse is our self-made struct to build response for Client
type BodyResponse struct {
	ResponseId      string                 `json:"responseId"`
	ResponseTime    string                 `json:"responseTime"`
	ResponseCode    string                 `json:"responseCode"`
	ResponseMessage string                 `json:"responseMessage"`
	ExtractData1    map[string]interface{} `json:"responseMessage"`
	ExtractData     interface{}            `json:"responseMessage"`
}

// Handler function Using AWS Lambda Proxy Request
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	datetime := time.Now().UTC()
	// BodyRequest will be used to take the json response from client and build it
	username := request.QueryStringParameters["username"]

	bodyResponse := BodyResponse{
		ResponseId:      uuid.New().String(),
		ResponseTime:    datetime.Format(time.RFC3339),
		ResponseCode:    "99",
		ResponseMessage: "System Err",
	}
	response, err := json.Marshal(&bodyResponse)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: string(response), StatusCode: 200}, nil
	}

	//verify datetime format RFC3339
	// parsedTime, err := time.Parse(time.RFC3339, bodyRequest.RequestTime)
	// if err != nil {
	// 	return events.APIGatewayProxyResponse{Body: err.Error() + "parsedTime: " + parsedTime.GoString(), StatusCode: 401}, nil
	// }

	//connect postgree
	const (
		host     = "rajje.db.elephantsql.com"
		port     = 5432
		user     = "yairsggo"
		password = "MbuwvGgJcC-nXskeCQnhunp8C93XC2-p"
		dbname   = "yairsggo"
	)
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+"password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	//	defer db.Close()
	err = db.Ping()
	if err != nil {
		panic(err)
	}
	sqlStatement := `DELETE FROM nhanpp1 WHERE username=$1`
	_, err = db.Exec(sqlStatement, username)
	if err != nil {
		bodyResponse = BodyResponse{
			ResponseId:      uuid.New().String(),
			ResponseTime:    datetime.Format(time.RFC3339),
			ResponseCode:    "04",
			ResponseMessage: "username not found",
		}
		response, err = json.Marshal(&bodyResponse)
		return events.APIGatewayProxyResponse{Body: string(response), StatusCode: 200}, nil
	}
	//
	bodyResponse = BodyResponse{
		ResponseId:      uuid.New().String(),
		ResponseTime:    datetime.Format(time.RFC3339),
		ResponseCode:    "00",
		ResponseMessage: "Success",
	}
	db.Close()
	sqlStatements := `DELETE FROM nhanpp1 WHERE username=$1`
	// exec error, connect close
	_, err = db.Exec(sqlStatements, username)

	// Marshal the response into json bytes, if error return 404
	response, err = json.Marshal(&bodyResponse)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 404}, nil
	}

	//Returning response with AWS Lambda Proxy Response
	return events.APIGatewayProxyResponse{Body: string(response), StatusCode: 200}, nil
}

func main() {
	// Handler("{\"requestId\":\"123456\",\"requestTime\":\"123456\",\"data\":{\"username\":\"nhanne_p\",\"name\":\"PhamPhuocNhan\",\"phone\":\"0902123456\"}}")
	// lambda.Start(Handler)
	anc()
}
func anc() {
	b := map[string]string{
		"": "",
	}
	a := BodyResponse{
		ExtractData: b,
	}
	typeOf := reflect.TypeOf(a.ExtractData)
	switch typeOf.Kind() {
	case reflect.Bool:
		fmt.Println("bool")
	case reflect.String:
		fmt.Println("string")
	// case reflect.Map:
	// 	fmt.Println("map")
	// 	isValid := reflect.ValueOf(a.ExtractData).IsValid()
	// 	if isValid {
	// 		fmt.Println("isValid")
	// 	} else {
	// 		fmt.Println("!isValid")
	// 	}
	default:
		fmt.Println("default")
		fmt.Println("map")
		reflect.ValueOf(a.ExtractData).NumField()
	}
}
