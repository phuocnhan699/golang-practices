package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"

	"encoding/json"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/google/uuid"
	_ "github.com/lib/pq"
)

// BodyRequest is our self-made struct to process JSON request from Client
type BodyRequest struct {
	RequestId   string      `json:"requestId"`
	RequestTime string      `json:"requestTime"`
	Signature   string      `json:"signature"`
	Data        DataRequest `json:"data"`
}

type ReqDataCheckPhone struct {
	Value int `json:"value"`
}

type BodyReqCheckPhone struct {
	RequestId string            `json:"requestId"`
	Data      ReqDataCheckPhone `json:"data"`
}

type DataRequest struct {
	UserName string `json:"username"`
	Name     string `json:"name"`
	Phone    string `json:"phone"`
}

// BodyResponse is our self-made struct to build response for Client
type BodyResponse struct {
	ResponseId      string `json:"responseId"`
	ResponseTime    string `json:"responseTime"`
	ResponseCode    string `json:"responseCode"`
	ResponseMessage string `json:"responseMessage"`
}

func getChar(str string, index int) rune {
	return []rune(str)[index]
}

// Handler function Using AWS Lambda Proxy Request
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	datetime := time.Now().UTC()
	// BodyRequest will be used to take the json response from client and build it
	bodyRequest := BodyRequest{
		RequestId: "",
	}

	bodyResponse := BodyResponse{
		ResponseId:      uuid.New().String(),
		ResponseTime:    datetime.Format(time.RFC3339),
		ResponseCode:    "99",
		ResponseMessage: "System Err",
	}
	response, err := json.Marshal(&bodyResponse)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: string(err.Error()), StatusCode: 200}, nil
	}

	err = json.Unmarshal([]byte(request.Body), &bodyRequest)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: string(err.Error()), StatusCode: 200}, nil
	}
	checkPhoneValue := string(getChar(bodyRequest.Data.Phone, len(bodyRequest.Data.Phone)-1))

	if checkPhoneValue == "" {
		bodyResponse.ResponseCode = "21"
		bodyResponse.ResponseMessage = "phone number not found"
		response, _ := json.Marshal(&bodyResponse)
		return events.APIGatewayProxyResponse{Body: string(response), StatusCode: 200}, nil
	}
	phoneNumber, err := strconv.Atoi(checkPhoneValue)
	if err != nil {
		return events.APIGatewayProxyResponse{Body: string(err.Error()), StatusCode: 200}, nil
	}
	// fmt.Println(phoneNumber)
	//call api check phone
	postBody, _ := json.Marshal(BodyReqCheckPhone{
		RequestId: uuid.New().String(),
		Data: ReqDataCheckPhone{
			Value: phoneNumber,
		},
	})
	responseBody := bytes.NewBuffer(postBody)
	fmt.Println("responseBody req check phone", responseBody)
	//Leverage Go's HTTP Post function to make request
	req, err := http.NewRequest(http.MethodPost, "https://1g1zcrwqhj.execute-api.ap-southeast-1.amazonaws.com/dev/testapi", responseBody)
	//Handle Error
	req.Header.Set("x-api-key", "B5d4JtTU8u1ggV8gp7OF88gcCGxZls6T3f5PYZSa")
	res, err := http.DefaultClient.Do(req)

	if err != nil {
		log.Fatalf("An Error Occured %v", err)
	}

	defer res.Body.Close()
	//Read the response body
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatalf("An Error Occured 2 %v", err)
	}
	sb := string(body)
	resCheckPhone := BodyResponse{}
	json.Unmarshal([]byte(sb), &resCheckPhone)
	fmt.Println("res:", resCheckPhone)
	//
	if resCheckPhone.ResponseCode != "00" {
		bodyResponse.ResponseCode = "01"
		bodyResponse.ResponseMessage = "invalid phone number"
		response, _ := json.Marshal(&bodyResponse)
		return events.APIGatewayProxyResponse{Body: string(response), StatusCode: 200}, nil
	}

	// Unmarshal the json, return 404 if error

	//create user api

	//call api check phone
	createUserPostBody, _ := json.Marshal(BodyRequest{
		RequestId:   uuid.New().String(),
		RequestTime: datetime.Format(time.RFC3339),
		Signature:   bodyRequest.Signature,
		Data: DataRequest{
			UserName: bodyRequest.Data.UserName,
			Name:     bodyRequest.Data.Name,
			Phone:    bodyRequest.Data.Phone,
		},
	})
	createUserResponseBody := bytes.NewBuffer(createUserPostBody)
	//Leverage Go's HTTP Post function to make request
	fmt.Println("createUserResponseBody:", createUserResponseBody)

	createUserReq, err := http.NewRequest(http.MethodPost, "https://jvo0fpqhbg.execute-api.us-east-1.amazonaws.com/user/create", createUserResponseBody)
	//Handle Error
	createUserReq.Header.Set("Content-Type", "application/json")

	createUserRes, err := http.DefaultClient.Do(createUserReq)

	if err != nil {
		log.Fatalf("An Error Occured %v", err)
	}

	defer createUserRes.Body.Close()
	//Read the response body
	createUserBody, err := ioutil.ReadAll(createUserRes.Body)
	fmt.Println("createUserBody", string(createUserBody))

	if err != nil {
		log.Fatalf("An Error Occured 2 %v", err)
	}
	createuserRes := BodyResponse{}
	json.Unmarshal([]byte(string(createUserBody)), &createuserRes)
	//
	fmt.Println("createuserRes", createuserRes)

	//verify uuid not null
	if bodyRequest.RequestId == "" {

		return events.APIGatewayProxyResponse{Body: "requestId can not be null", StatusCode: 200}, nil
	}

	// bodyResponse = BodyResponse{
	// 	ResponseId:      uuid.New().String(),
	// 	ResponseTime:    datetime.Format(time.RFC3339),
	// 	ResponseCode:    "00",
	// 	ResponseMessage: "Success",
	// }
	// Marshal the response into json bytes, if error return 404
	response, err = json.Marshal(&createuserRes)
	fmt.Println("response:", string(response))

	if err != nil {
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 404}, nil
	}

	//Returning response with AWS Lambda Proxy Response
	return events.APIGatewayProxyResponse{Body: string(response), StatusCode: 200}, nil
}

func checkPhone(phoneNumber string) (bool, error) {
	var err error

	return false, err
}

func main() {
	// if _, err := Handler(events.APIGatewayProxyRequest{
	// 	Body: "{\"requestId\":\"123456\",\"requestTime\":\"123456\",\"signature\":\"497cd1f003812a5fb06484a9bc5996020753d95cfb988f38a6a58dab247048eb\",\"data\":{\"username\":\"nhan1\",\"name\":\"nhanpham\",\"phone\":\"0902505662\"}}",
	// }); err != nil {
	// 	fmt.Println(err.Error())
	// }
	lambda.Start(Handler)
}
