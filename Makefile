.PHONY: build clean deploy

build:
	env GOARCH=amd64 GOOS=linux CGO_ENABLED=0 go build -ldflags="-s -w" -o bin/buoi3/createUserV2 buoi3/createUserV2/main.go
	env GOARCH=amd64 GOOS=linux CGO_ENABLED=0 go build -ldflags="-s -w" -o bin/user/create user/create/main.go
	# env GOARCH=amd64 GOOS=linux CGO_ENABLED=0 go build -ldflags="-s -w" -o bin/user/delete user/delete/main.go
	# env GOARCH=amd64 GOOS=linux CGO_ENABLED=0 go build -ldflags="-s -w" -o bin/user/update user/update/main.go
	# env GOARCH=amd64 GOOS=linux CGO_ENABLED=0 go build -ldflags="-s -w" -o bin/user/get user/get/main.go

clean:
	rm -rf ./bin

deploy: clean build
	sls deploy --verbose --aws-profile cloudguru
