module github.com/aws/aws-lambda-go/events

go 1.18

require (
	github.com/aws/aws-lambda-go v1.41.0
	github.com/google/uuid v1.3.0
)

require github.com/lib/pq v1.10.9 // indirect
